---
category: life
created: 2017.01.01:1500
title: Years In Review
type: page
updated: 2019.01.01:0750
---

## 2018

### Favorite Games

One of the central themes for gaming this year has been games with strong soundtracks. All four of my favorite games have soundtracks that I regularly listen to. They are also games that are very deep, and ones I plan to revisit very soon to invest more time in.

<figure>
    <img src='/images/years-in-review_final-fantasy-xv.jpg'>
    <figcaption>Final Fantasy XV</figcaption> 
</figure>

For a game that had such a troubled history, Final Fantasy XV turned out to be one of my favorite Final Fantasy games in the whole series. It is a flawed game, but it hits more high points than low. Yoko Shimomura's soundtrack is the biggest element that kept me reeled in. Her music stands up with the best of Uematsu's, and without it, I don't think I would have enjoyed the game quite as much as I did.

<figure>
    <img src='/images/years-in-review_subnautica.jpg'>
    <figcaption>Subnautica</figcaption> 
</figure>

The survival genre is one I have passed on previously. Most survival games look more tedious than fun, and not focused on the action. The only reason I gave Subnautica a chance is that it is made by Unknown Worlds, the creators of one of my favorite multiplayer games, Natural Selection. As great of a game as Natural Selection was, Subnautica was even better. Everything design-wise in the game is perfect, from the story, survival mechanics, base building, and sound and visual design.

The experience moved me so much that while I want to play it again, I am afraid that the second time around will not feel as magical due to the lack of discovery. But I am looking forward to the stand-alone expansion being released next year, and hope it can meet the greatness of this first game.

<figure>
    <img src='/images/years-in-review_exapunks.jpg'>
    <figcaption>Exapunks</figcaption> 
</figure>

Zachtronics games have a certain quality that no other "programming game" has matched. Most of their games are based around an artificial programming language or platform that ends up looking like programming on the surface but is really a puzzle game. All of their games are fun, but Exapunks takes things to the next level. The soundtrack and graphics are better than anything Zachtronics has put out before. The game has a distinctive 1980's cyberpunk vibe to it, which fits with the William Gibson and Neal Stephenson novels I have been reading this year. Like Shenzhen I/O before it, I expect to come back to this game again in a few years.

<figure>
    <img src='/images/years-in-review_pillars-of-eternity-ii-deadfire.jpg'>
    <figcaption>Pillars of Eternity II: Deadfire</figcaption> 
</figure>

Games from Obsidian Entertainment rarely disappoint, and Pillars of Eternity II is no except to this. The first game was a great return to form for the CRPG genre, and the sequel takes it to all new heights. Everything in the game was done well, from the story to the combat to the music. With Obsidian's buy-out by Microsoft, its unclear whether another Pillars of Eternity game will come out. As it stands, the series stands amongst the best of the CRPG genre.

### Favorite Albums

- Exapunks, by Matthew S Burns
- Final Fantasy XIV: Stormblood, by Masayoshi Soken
- Final Fantasy XV: Volume 2, by Yoko Shimomura
- Gris, by Berlinist
- The Legend of Zelda: Breath of the Wild, by Manaka Kataoka, Yasuaki Iwata, and Hajime Wakai
- Mid90s, by Trent Reznor and Atticus Ross
- Octopath Traveler, by Yasunori Nishiki
- 7 Billion Humans, by Kyle Gabler

<figure>
    <img src='/images/years-in-review_music-2018.jpg'>
    <figcaption>Favorite Music of 2018</figcaption>
</figure>

### A Multitude of Interfaces

Up to the release of macOS Mojave, I spent a lot of time experimenting with keeping as much of my work inside of a terminal as possible. I rewrote scripts and workflows to be friendly to a terminal session, and became comfortable enough with neovim to use it as a primary text editor. Having grown up using MS-DOS and being comfortable with a Unix command-line for a couple of decades, doing this was not a painful experiment. But while there are some advantages of working within the terminal, there are a number of disadvantages to it as well.

Upon some deep reflection, I concluded that the reason I kept being drawn to the terminal was not due to any inherent advantage of the terminal itself, but due to my unhappiness with the current macOS GUI setup. macOS has been my operating system of choice since college, but the current trends of desktop UI design were not catching on with me. The macOS desktop design was not stagnating, but it was also not evolving in a way that meshed with what I was looking for.

When I realized what made me unhappy about the GUI and what I enjoyed with the terminal, I set out to try and combine the two in a meaningful way. In practice, this meant taking the keyboard-centric interactions of the terminal and applying them to GUI applications. This required some careful selection of applications, although a lot of what I already used was friendly to this.

At the end of this experimentation, I am much happier with how I use my Mac. I kept drawing a distinction between the terminal and GUI environment, but taking the advantages of both sides and figuring out how they can be applied to each other has been fun and worthwhile. It also makes me more comfortable with the direction Apple currently is on with the operating system as a whole, as my workflow might require a little more effort, but it still works well and should continue to do so into the future.

## 2017

### Favorite Games

When I looked at the upcoming titles of 2017 back in January, I thought this year was going to be a boring year for games. Last year saw some instant classics for myself, and 2017 seemed like it would underperform in comparison. Thankfully, there were some great games this year that I look forward to playing through again in the future.

<figure>
    <img src='/images/years-in-review_prey-2017.jpg'>
    <figcaption>Prey</figcaption> 
</figure>

Earlier this year, I played [System Shock 2](https://en.wikipedia.org/wiki/System_Shock_2) from beginning to end for the first time. It is an older game, but considered a classic of the FPS/RPG hybrid genre. The initial reviews for [Prey](https://en.wikipedia.org/wiki/Prey_(2017_video_game)) kept saying it was the spiritual successor to System Shock 2, and as close as we'll get to a System Shock 3 anytime in the near future.

These reviews were not exaggerations either. Prey is every bit the same as System Shock 2, including in quality. The gameplay is the perfect blend of FPS and RPG, and it has the same overall game structure as System Shock 2. The open-ended nature of the game is why I love the Deus Ex games so much. Prey is a game that I have already slotted in to play again in the future, and much like with any Deus Ex game, I plan to approach it in a new manner on the second playthrough.

<figure>
    <img src='/images/years-in-review_final-fantasy-xiv-stormblood.jpg'>
    <figcaption>Final Fantasy XIV: Stormblood</figcaption>
</figure>

This year saw the release of Stormblood, the second expansion for [Final Fantasy XIV](https://en.wikipedia.org/wiki/Final_Fantasy_XIV:_A_Realm_Reborn). Final Fantasy XIV is one of my top MMORPGs of all time, and Stormblood keeps going with the same formula that has made the original game amazing. The story has been wonderful to follow along with, the dungeons and raids fun, and the new mechanics add some more depth to the already spectacular combat.

<figure>
    <img src='/images/years-in-review_what-remains-of-edith-finch.jpg'>
    <figcaption>What Remains of Edith Finch</figcaption>
</figure>

My year would not be complete without a new [walking simulator](https://en.wikipedia.org/wiki/Adventure_game#Exploration_games) to play through. [What Remains of Edith Finch](https://en.wikipedia.org/wiki/What_Remains_of_Edith_Finch) is my walking simulator of choice for 2017, and what an amazing game it is. It surpasses every other in the genre, including the perennial [Dear Esther](https://en.wikipedia.org/wiki/Dear_Esther).

<figure>
    <img src='/images/years-in-review_life-is-strange-before-the-storm.jpg'>
    <figcaption>Life Is Strange: Before the Storm</figcaption>
</figure>

[Life Is Strange: Before the Storm](https://en.wikipedia.org/wiki/Life_Is_Strange:_Before_the_Storm) is how prequels should be done. [Life Is Strange](https://en.wikipedia.org/wiki/Life_Is_Strange) is a good but flawed game, and Before the Storm improves on it in most categories. The dialogue between the characters runs the emotional gamut, and the art style of the game makes it quite visually pleasing. There are so many memorable scenes in the game, from Episode 1's Dungeons and Dragons session, to the high school play in Episode 2. The first game is one I have no plans to revisit again, but Before The Storm is one I know I'll play again in the future.

### Favorite Albums

- Derelict, by Carbon Based Lifeforms
- Deus Ex: Breach, by Ed Harrison
- The Elder Scrolls Online: Morrowind, by Brad Derrick
- Ligand, by Martin Nonstatic
- Luna, by Austin Wintory
- Prey, by Mick Gordon
- The Vietnam War, by Trent Reznor
- What Remains of Edith Finch, by Jeff Russo

<figure>
    <img src='/images/years-in-review_music-2017.jpg'>
    <figcaption>Favorite Music of 2017</figcaption>
</figure>

### Using Swift to run my life

Every since I first tried it out in high school, programming has been an activity that I have enjoyed immensely. Whether it's working on a bigger project for myself or just solving some simple math puzzles, programming is a great way for me to relax. It has also been a phenomenal tool for organizing my life and experimenting with different work routines.

Last year, I felt that Swift was not in a place where I wanted to use it regularly. I loved the language itself, but the tooling around it did not interest me. It was next to impossible to integrate well with Sublime Text and Xcode was so bloated that I did not enjoy opening it, let alone using it.

This year saw a lot of change in regards to the Swift tooling. With the release of Swift 4, the Swift Package Manager has become more powerful and easy to work with, and Xcode 9 brought speed improvements to the editor, making it so that I could better cope with working in it. A good portion of this year was spent experimenting with different programming tools, and this included the beta versions of Xcode 9. I discovered that Xcode is now in a place where I can combine it with using the Swift Package Manager from the command-line and be happy with the workflow. Consequently, Xcode is now the main editor I use for all Swift work. I do not use many of the additional IDE tools it provides, although it does make a great debugger on the occasions that I need it.

One thing I discovered this year is that as much as I would love to create apps for iOS and macOS, it is just not something I enjoy doing. I am more of a command-line person, so whatever tooling works best there is what I want to learn. Python and Rust are great for this usage, but Swift has also turned into a solid entry as well. When Apple first announced Swift, they partially billed it as a programming language. Despite being sort of garbage-collected, it has great performance and provides all of the tools necessary to great command-line programs. It also works well in a scripting capacity, although it is yet to be as fast as Python in that area.

All of this adds up to Swift having become my primary language for my personal projects. Python is out. Rust is a great language, but not one I feel I can comfortably devote time to while still being productive, as I sometimes have limited hours in the day to work on programming projects. Improved Linux support means that if I switch to Linux, I will be able to take the language with me, which is something that is always at the back of my mind.

### Replacing my iPhone with an iPad

I have wanted an iPad ever since they were first announced. iOS is a great operating system, but I have become more disillusioned with smartphones as corporations and app developers use every tool at their disposal to make apps "addictive" and something that you must come back to regularly, instead of a just a tool to use. Sorting through the trash of these apps takes more and more time that would be better spent on other things.

When I bought an iPad during the summer, I decided to run an experiment: allow my phone to be a glorified iPod, and convert all the rest of my mobile computing to the iPad. This might not seem like a huge change, but it has actually changed my life in ways that I did not at first imagine.

The big change came from the fact that I can't simply pull out my iPad everywhere. Everywhere in public, people are always using their smartphones for random tasks. Bored on the train? Pull our your phone. Waiting simply 30 seconds in line at the grocery? Pull out your phone. Bored of the conversation you are having with a co-worker? Pull out your phone. I fell into the same trap of doing this in many of the same situations.

To try and combat this, I removed all apps from my iPhone except for music, podcasting, and basic communication tools(phone calls and texting). E-mail is delayed on there, and outside of texting, no other messaging apps are installed. I even went through the trouble to make Safari inaccessible. What I found is that when I am out of the house, I notice more things around me. Music is still playing in my ears, but I can visually see a lot more. It also gives me more time to just think about things, instead of trying to quickly beat a level on a mobile game designed to suck money out of me.

This has also helped me keep the iPad more of a focused device for reading and researching. Since I no longer am used to doing random stuff on my iPhone, I do not feel the inclination to do it on the iPad. Even mobile gaming has dropped for me, as the money-seeking games on the iPhone are the same on the iPad.

The iPad is great in the roles I have relegated it to. iBooks makes reading a wonderful task, where I prefer it there over paper books. Writing is equally nice, when I have the time to do some writing projects on it. And the focused nature of full-screen apps is one I long ago adopted on my Mac. Even as I write this in Sublime Text, it is set to fullscreen and most UI elements are turned so I can focus on the text.

One financial gain from this is that I no longer desire to spend nearly a thousand dollars on a new phone every couple of years. My 3 year old iPhone 6 Plus is still more than adequate for my needs, and when I do decide to upgrade this again, it will be to a iPhone SE that is both smaller and cheaper. More productive time on my devices, more knowledge of what is going on around me, less money spent. This transition has had many positive changes for me this year.

## 2016

For many reasons, 2016 has been a rollercoaster of a year. While keeping any political and social issues aside, the following some of the good things I have experienced this year, with some closing thoughts on a couple of areas I want to  do better with next year.

### Favorite Games

2016 was a phenomenal year for games. Here are a few of my favorites.

<figure>
    <img src='/images/years-in-review_deus-ex-mankind-divided.jpg'>
    <figcaption>Deus Ex: Mankind Divided</figcaption>
</figure>

The original [Deus Ex](https://en.wikipedia.org/wiki/Deus_Ex_(video_game)) was an amazing game for its time. It opened my eyes to the idea that there could be more to a first-person shooter. [Deus Ex: Human Revolution](https://en.wikipedia.org/wiki/Deus_Ex:_Human_Revolution), is as good of a sequel as one could ask for, and is now one of my favorite all-time games, having played through multiple times.

This year's direct sequel to the series, [Deus Ex: Mankind Divided](https://en.wikipedia.org/wiki/Deus_Ex:_Mankind_Divided) is a great game that barely does not measure up to Human Revolution. Where the game falls down is in the ending and in how the story handles the conspiratorial Illuminati. In the original game, they are mentioned briefly and play a behind-the-scenes role. In this game, they are at the forefront, and the main characters are searching for them specifically. It removes some of the enigma of the group and who they are, which Human Revolution handled better.

Outside of those two minor quips, Mankind Divided is a game that I am happy is more of the same. It does not veer far from what Human Revolution did, with a similar game structure and ability system. The new characters are well-written, and the missions and side-quests are a lot of fun. This is another game which I plan to play through again in the near future.

<figure>
    <img src='/images/years-in-review_doom.jpg'>
    <figcaption>DOOM</figcaption>
</figure>

The last game from [id Software](https://en.wikipedia.org/wiki/Id_Software) that I enjoyed was [Quake III: Arena](https://en.wikipedia.org/wiki/Quake_III_Arena), released 17 years ago. [DOOM 3](https://en.wikipedia.org/wiki/Doom_3) and [Rage](https://en.wikipedia.org/wiki/Rage_(video_game)) were decent games in their own right, but while they continued to show [John Carmack's](https://en.wikipedia.org/wiki/John_Carmack) excellence in graphic engine design, they lacked the magic that id's previous games held.

This is why the release of [DOOM](https://en.wikipedia.org/wiki/Doom_(2016_video_game)) was surprising. After coming off the lackluster multiplayer beta, my hopes were low for the game.

And then I played it.

Ranking DOOM as my game of the year is an easy decision. While not as revolutionary as the original, this DOOM is every bit as good of a game. Every aspect of this game is near perfect: beautiful graphics that runs smooth, an amazing soundtrack by [Mick Gordon](https://en.wikipedia.org/wiki/Mick_Gordon), memorable level design, and fun gameplay that kept me coming back for me. It is a game that I look forward to playing again in the future.

<figure>
    <img src='/images/years-in-review_overwatch.jpg'>
    <figcaption>Overwatch</figcaption>
</figure>

There is little that I can say about [Overwatch](https://en.wikipedia.org/wiki/Overwatch) that has not been said elsewhere. Suffice it to say, this game took the FPS world by storm this year. From interesting and well thought out characters to the tight gameplay, it is easy to see why most love this game. It is the one game I have [logged in the most hours on](https://playoverwatch.com/en-us/career/pc/us/Krueger-1750), and still continue to love playing 7 months later.

<figure>
    <img src='/images/years-in-review_the-witness.jpg'>
    <figcaption>The Witness</figcaption>
</figure>

[Jonathan Blow's](https://en.wikipedia.org/wiki/Jonathan_Blow) previous game, [Braid](https://en.wikipedia.org/wiki/Braid_(video_game)), did little for me. It was an interesting concept, but platformers in that style are hard for me to get into. Consequently, I did not take a close look at [The Witness](https://en.wikipedia.org/wiki/The_Witness_(2016_video_game)) until it was near release. And I was enthralled by what I saw.

This game got stuck inside my head once I started playing it. I was thinking about it no matter what I was doing. I even started waking up early in the morning just to play it more. Few games are like that for me.

It is a hard game to describe without spoiling the experience. Needless to say, if you enjoy puzzle games and exploration, this is one to get.

### Favorite Music

<figure>
    <img src='/images/years-in-review_music-2016.jpg'>
    <figcaption>Favorite Music of 2016</figcaption>
</figure>

### Cipher, by Ben Prunty

Ever since the release of his soundtrack for [FTL](https://en.wikipedia.org/wiki/FTL:_Faster_Than_Light), [Ben Prunty](https://benprunty.com) has been one of my favorite composers of video game soundtracks. His [latest soundtrack](https://benprunty.bandcamp.com/album/cipher-the-score-for-banking-on-bitcoin) for the documentary [Banking On Bitcoin](http://www.imdb.com/title/tt5033790/) is one of his best works yet. Much like the rest of Prunty's work, it is the perfect soundtrack for long coding sessions.

### DOOM, by Mick Gordon

Much like the game itself, the [soundtrack for DOOM](http://mick-gordon.com/doom/) is amazing. [Mick Gordon](http://mick-gordon.com) is a name I was unfamiliar with before the release of the game, but instantly fell in love with his work.

Gordon took a different direction with the music compared to [Robert Prince's](https://en.wikipedia.org/wiki/Robert_Prince_(video_game_composer)) iconic soundtracks for DOOM and DOOM II. Instead of songs reminiscent of 1980's and early 1990's metal, Gordon created a piece of work more inline with the work of [Trent Reznor's](https://en.wikipedia.org/wiki/Trent_Reznor) [Quake](https://en.wikipedia.org/wiki/Quake_(video_game)) soundtrack. Very industrial in sound, it is music that stands well on its own. I have listened to the official release and various fan-edits numerous times since it came out, and I can say it stands up to, if not surpasses, the great work of all previous DOOM and Quake soundtracks.

### Far & Off, by AES Dana and Miktek

[AES Dana](http://ultimae.com/artists/aes-dana/) and [Miktek](https://miktek.bandcamp.com) are two of my favorite ambient artists. When I heard that they were releasing an album together, I was ecstatic.

[Far & Off](https://ultimae.bandcamp.com/album/far-off-2) is the compilation of 3 separate EPs released by the two throughout the year: [Alkaline](https://ultimae.bandcamp.com/album/alkaline), [Cut](https://ultimae.bandcamp.com/album/cut), and [The Unexpected Hours](https://ultimae.bandcamp.com/album/the-unexpected-hours). All of the tracks are low tempo and ethereal in nature, and it makes great meditation music, or something to relax to.

### Moonbathers, by Delain

Every album [Delain](https://en.wikipedia.org/wiki/Delain) has released has been phenomenal, and [Moonbathers](https://en.wikipedia.org/wiki/Moonbathers) is no exception. Out of all [power](https://en.wikipedia.org/wiki/Power_metal) and [symphonic metal](https://en.wikipedia.org/wiki/Symphonic_metal) albums released this year, this is the one with the fewest songs I skip on it. This might seem like an odd metric, but in today's music, it is not a given that a full album will be good. Even some of my favorite artists regularly put out albums that have filler content that I skip over. Thankfully, this Delain album is not one of those.

### Programming

This has been a lackluster year programming-wise for me. [Swift](https://swift.org) did not take off for me, mostly due to my dislike for its current tooling setup. HTML and CSS have had little interesting happening in them, outside of better support for Flex Box and Grid Layout. And JavaScript continues to show little reason for me to desire working with it.

The one area where my interest is rising is with [Python](https://www.python.org). It is not my perfect language, but it does a lot right that Swift does not, particularly in the tooling. I am a big fan of lightweight code editors such as [Sublime Text](https://www.sublimetext.com), and having to work in Xcode turned me off of Swift. Whereas with Python, there are a lot of plugins for Sublime to help make Python programming easier, while still remaining lightweight. It also has a huge community following and packages for just about everything, which means I can easier work on projects that I want to work on.

I do plan to stick with Python for some of my work going forward, at least for my own website. While I am happy with the current solution I have, something more powerful that I can add on to would be helpful.

Hopefully I do not get distracted by the allure of [Haskell](https://www.haskell.org) or [Rust](https://www.rust-lang.org/en-US/).

### Scheduling

When I was working part time, it was easy to fit everything that I wanted into a day. Most days, I only worked for 6 hours, which left plenty of time for gaming, programming, and Whitney. Now, with 8 hour shifts, lots of overtime, and a lengthier commute, I need to spend time managing my time. Throughout this year, I have worked on exactly that.

There are many ways to try and manage your time, with just as many books written to guide you through that process. [GTD](https://en.wikipedia.org/wiki/Getting_Things_Done) is one of the more popular methods, and one that I experimented with for a few months.

This is still something that I am working on, but I have found what works for me and a lot of what does not work. I have focused what I want to keep my skills up with, how I want to have fun, and the best way to balance the two. My daily tasks are split into a few groups, some of which are scheduled at specific times based on GTD, and others that are from a checklist with repeating tasks.

While I still am tweaking the system, it is mostly in place for this. I plan to write about it within the coming months, as each piece is finalized.

### The Krueger Report

Figuring out the best way to run this site with minimal upkeep has been a challenge for a few years now. When I first made this site, my intention was to stay away from typical bloated CMS's like [WordPress](https://wordpress.org). These applications work well, but I wanted something that put less strain on the server. For a short while in 2015, I ran on top of [Ghost](https://ghost.org), which enticed me with its native Markdown support and ability to run without a database server. It worked well, and out of all CMS's I have used, it is my favorite. But it was still too much.

After converting the site into static HTML, I went about trying to find the best solution for compiling down the site based on Markdown files. I first attempted to create my own solution, but this entailed more work than I was willing to give at the time. I tried maintaining the HTML files myself, but this turned into a long process every time I wanted to post something.

The solution I ended up implementing a few months back was to switch from [nginx](http://nginx.org) to [Caddy](https://caddyserver.com). Caddy has the built-in ability to compile Markdown files to HTML based upon a template system, which appealed to me. Its system was similar to how Ghost themes were made, so it was quick to adapt my site to it. Caddy also has the huge bonus of taking care of SSL certificates for all of my sites, meaning I do not have to manually register and renew them.

I still have visions of a static site compiler that has other functionality(compiling the book portion down to an [EPUB](https://en.wikipedia.org/wiki/EPUB) for e-book reading), and if that happens, it will most likely be in Python. But we'll see if I get around to it.

### What Was Left Behind

Ever since playing the [SNES](https://en.wikipedia.org/wiki/Super_Nintendo_Entertainment_System) classic [Chrono Trigger](https://en.wikipedia.org/wiki/Chrono_Trigger), I have badly desired a real-world equivalent to the [Enertron](https://www.chronocompendium.com/Term/Enertron.html) found in the game. These devices would let the characters experience a full night's worth of rest in only a few seconds.

If I had a device like this, I would be able to work on a lot more projects than I currently have time for. Without that though, I have had to focus on what I want to do, which means putting some hobbies and projects off.

When the year began, I had hoped to get back into playing bass guitar and perhaps look into joining a band. One of the best parts of college was our old band Unabridged, and performing for others. Unfortunately, this is one hobby that I have not been able to fit into my schedule. This mostly stems from the fact that I know I would have to sacrifice other activities to take it to the level I would want to. Writing music can take up a lot of time and mental energy, which I want to direct at other activities. I have thought about joining a cover band or work on a project where I am simply a player and do not need to write, but then I question whether that would be as much fun.

One project that has been bouncing around in my head for nearly a decade now is writing a fiction novel. I used to love writing when I was in college, and would regularly write short stories and work on a novel, but never took it to completion. Since I graduated, the frequency with which I wrote lessened, to the  point where I had a number of years where I wrote nothing at all.

One reason that I have been working on this blog the last few years was to get myself back into the habit of writing, and to rebuild my ability to do it. I used to be able to write well-thought out texts with ease, but now it is more of struggle. With the practice of writing these last few years, the process of writing is easier for me. Perhaps it is time to approach that novel again.

Over the next few months, I will make a decision if either of these projects are worth exploring fully. While I can not do everything that I would look to, I want to try to do more than I currently am doing. With focus, I will hopefully be able to.
