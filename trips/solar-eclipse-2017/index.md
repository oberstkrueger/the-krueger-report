---
category: trips
created: 2017.08.22:0830
title: Solar Eclipse - 2017
type: page
updated: 2018.04.12:0040
---

On August 21, 2017, a [solar eclipse passed over the US](https://en.wikipedia.org/wiki/Solar_eclipse_of_August_21,_2017). Astronomy events such as this are fun to watch, but this particular eclipse became such a huge event that traveling far for it would be difficult and time consuming. We did not have to go far to have a great viewing experience.

<figure>
	<img src='/images/solar-eclipse-2017_times.jpg'>
	<figcaption>Solar eclipse information for Tualatin Hills Nature Park</figcaption>
</figure>

99.45% coverage of the sun won't darken the sky enough to see the stars, but it is enough to see everything darken significantly. We picked a park that had enough tree coverage to obscure the sun. This meant that we were there more for the ambiance of the eclipse and not for viewing it directly. This worked out to our advantage though, as we ran into few other people within the park.

<figure>
	<img src='/images/solar-eclipse-2017_map.jpg'>
	<figcaption>The map of Tualatin Hills Nature Park</figcaption>
</figure>

<figure>
	<img src='/images/solar-eclipse-2017_path.jpg'>
	<figcaption>The trail through the park</figcaption>
</figure>

<figure>
	<img src='/images/solar-eclipse-2017_spider-web.jpg'>
	<figcaption>There were aa great number of large spider webs throughout the park</figcaption>
</figure>

Our final destination within the park was Big Pond. This provided a view over a lake filled with ducks. We arrived about an hour before the eclipse started, and the entire time we were there only heard one other couple nearby.


<figure>
	<img src='/images/solar-eclipse-2017_big-pond.jpg'>
	<figcaption>Big Pond, complete with a nice bench to sit on and watch from</figcaption>
</figure>

<figure>
	<img src='/images/solar-eclipse-2017_lake.jpg'>
	<figcaption>A view of the lake</figcaption>
</figure>

<figure>
	<img src='/images/solar-eclipse-2017_deer.jpg'>
	<figcaption>While awaiting the eclipse, a deer passed by</figcaption>
</figure>

<figure>
	<img src='/images/solar-eclipse-2017_near-peak.jpg'>
	<figcaption>An iPhone can not fully capture the extent of the darkness, but it can get close</figcaption>
</figure>
