---
category: trips
created: 2017.08.14:1100
title: Walt Disney World - 2016
type: page
updated: 2018.04.12:0040
---

### The Grand Floridian and Boardwalk Villas

For all of my previous trips to Walt Disney World, I have always stayed at the [All-Star Sports Resort](https://en.wikipedia.org/wiki/Disney%27s_All-Star_Sports_Resort). This time, the hotels of choice consisted of the [Grand Floridian](https://en.wikipedia.org/wiki/Disney%27s_Grand_Floridian_Resort_%26_Spa) and [Boardwalk Villas](https://en.wikipedia.org/wiki/Disney%27s_BoardWalk_Resort#Villas). The Grand Floridian is a beautiful hotel and I would love to stay there again in the future. The quality of the hotel matched that of the [Grand Californian](https://en.wikipedia.org/wiki/Disney%27s_Grand_Californian_Hotel_%26_Spa) at Disneyland, which is my current favorite Disney resort.

<figure>
	<img src='/images/walt-disney-world-2016_grand-floridian.jpg'>
	<figcaption>View from our room at the Grand Floridian</figcaption>
</figure>

<figure>
	<img src='/images/walt-disney-world-2016_boardwalk.jpg'>
	<figcaption>The Boardwalk Inn and Villas as seen from Disney's Yacht Club<figcaption>
</figure>

Of note around the resorts is the number of lizards that call Walt Disney World their home. During most walks into the hotels, many of these lizards will be scampering about along the pathways.

<figure>
	<img src='/images/walt-disney-world-2016_lizard.jpg'>
	<figcaption>A lizard along the walls at the Grand Floridian</figcaption>
</figure>

### The Sci-Fi Dine-In Theater

One of best parts of this trip was discovering the [Sci-Fi Dine-In Theater](https://en.wikipedia.org/wiki/Sci-Fi_Dine-In_Theater_Restaurant). Normally, a restaurant would not rate too highly up on my list of things that I enjoyed, but this restaurant had everything down perfect. The theme of the restaurant is in the style of old [drive-in theaters](https://en.wikipedia.org/wiki/Drive-in_theater). The food here was excellent, and the ambiance of cheesy sci-fi movies from the 1950s and 1960s made for some wonderful meals.

<figure>
	<img src='/images/walt-disney-world-2016_theater-cars.jpg'>
	<figcaption>The tables all look like convertible cars</figcaption>
</figure>

<figure>
	<img src='/images/walt-disney-world-2016_theater-tables.jpg'>
	<figcaption>Another view of the seating arrangements</figcaption>
</figure>

<figure>
	<img src='/images/walt-disney-world-2016_theater-screen.jpg'>
	<figcaption>Old-school sci-fi makes for great meal-time entertainment</figcaption>
</figure>

### Fantasmic

The version of Fantasmic at Walt Disney World is not up to the standard set by the original Fantasmic at Disneyland, but it is still a great show to watch. One of the advantages that this version has is that it takes place in a theater setup specifically for the show. Everyone who is watching the show gets to sit down without any need to simply find a spot and stand to watch.

<figure>
	<img src='/images/walt-disney-world-2016_fantasmic-dragon.jpg'>
	<figcaption>Maleficent lights the river on fire</figcaption>
</figure>

<figure>
	<video src="/images/walt-disney-world-2016_fantasmic-finale.mp4" controls preload="metadata"></video>
</figure>

### The Tower of Terror

[The Twilight Zone Tower of Terror](https://en.wikipedia.org/wiki/The_Twilight_Zone_Tower_of_Terror) remains my favorite ride at the resort. It is quintessential Disney theming combined with a great ride. With the Disneyland version being replaced by a [Guardians of the Galaxy ride](https://en.wikipedia.org/wiki/Guardians_of_the_Galaxy_–_Mission:_Breakout!),  Walt Disney World is now home to both the original and best version of the ride.

<figure>
	<img src='/images/walt-disney-world-2016_tower.jpg'>
	<figcaption>The Twilight Zone Tower of Terror</figcaption>
</figure>
