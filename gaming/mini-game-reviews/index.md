---
category: games
created: 2018.12.02:1750
title: Mini Game Reviews
type: page
updated: 2019.04.07:1010
---

## Counter-Strike: Global Offensive

The first beta of the original Counter-Strike was released back in 1999. I did not play this first version, instead hopping in a few betas later. The original Counter-Strike is a great game, and was a core game that I played throughout most of high school years and into college.

What impresses me the most about Counter-Strike: Global Offensive is that it is the 3rd major iteration of the game, and it sticks to the formula that made the beta versions of the original Half-Life mod so successful. The game has experienced technological upgrades, but the core gameplay loop is the same was it was 20 years ago. Unlike other game series, the technology around the game modernizes while the gameplay itself is still the same.

This is emphasized by looking at the game's most popular map: de_dust2. The original de_dust2 was released in 2001, and quickly became a mainstay on servers. It is a well-balanced map that has not needed any major gameplay tweaks since the original version. In Counter-Strike and Counter-Strike: Source, there were servers dedicated to playing the map 24/7. Counter-Strike: Global Offensive honors this by having a map group dedicated to solely playing de_dust2, giving the same functionality.

Counter-Strike is a game that I still enjoy, although I am nowhere near as good as I was back in high school. But it is still fun to play, and something I see myself going back to every so often just to experience it again. Valve's stewardship of the Counter-Strike series is impressive, and I hope that they continue to not make any fundamental changes to the series as a whole. Counter-Strike could easily remain one of the main games that everyone will have played decades into the future, if Valve continues to treat it as it currently does.

<figure>
    <img src='/images/mini-game-reviews_counter-strike-global-offensive.jpg'>
</figure>

## Diablo 3

Diablo 2 is the quintessential action RPG. Many other games are modeled after what Diablo 2 did well. Of Blizzard's catalog of games, Diablo 2 is the one that I think holds up the best and has few weak points to it. So naturally, Blizzard approached creating a sequel to the game by changing the gameplay, music, and visual style that Diablo 2 had perfected.

There is a lot I dislike about Diablo 3, but there are two main points that create the core of my problems with the game. The first point is that random drops have been replaced by random everything. Not only are items pulled from a list of unique and static items, but now even the unique items have randomized stats. In Diablo 1 and 2, if you wanted a particular piece of equipment to drop, your journey was over for that item. In Diablo 3, it is possible to get a specific item to drop with random stats that are horrible, or to get the proper stats but always know that another drop with another random roll might make the stats a little better. The second point is that Diablo 3 scaled everything up: damage, item values, health pools, etc. Damage is now shown in numbers that pop up on the screen, showing how well you are hitting. As you progress through the game, the numbers continue to grow larger, reaching ridiculous levels like thousands of points of health and millions of points of damage. Both of these points combine to make the game feel like a digital slot machine, where you move your character around, click your mouse, and see how big the numbers are that show up on the screen.

Despite improvements and new content that Blizzard has added to the game since launch, they have been unable to turn it around into a game that I can enjoy. It is one of the few games in my life that has put me to sleep before due to how boring it was. This is such a stark contrast to the excitement that Diablo 2 was. Given Blizzard's business moves in recent years, Diablo 3 is the last chance I'll be giving the studio on a game in the Diablo franchise.

<figure>
    <img src='/images/mini-game-reviews_diablo-3.jpg'>
</figure>

## Gris

When I was playing through Gris, I was reminded of my first playthrough of Journey. The sense of wonder that the world had combined with the emotional experience of the game to create an unforgettable experience. The game's theme is presented in an abstract manner, but my take on it is dealing with the loss of a loved one. As the main character progresses through the game, she explores the depths of her own depression. Hints of this are seen in the game's achievements, which are named after various stages of grief.

The most memorable part of Gris is its visuals. Ever since the old platformers of the 1990s, there has been a hope of having games that look like a piece of hand-drawn art brought to life. Gris accomplishes this goal beautifully. All of the art is hand-drawn, with the characters having a pencil-drawn look, and the world being created through splashes of watercolor. Environmental effects look like paint spreading across the screen, creating a very otherworldly effect.

The gameplay of Gris is satisfying, albeit not the highlight of the game. The platform mechanics are lacking compared to other platformers. But since this game is more about the emotional and visual journey, the lack of strong platforming is not something I hold against the game.

<figure>
    <img src='/images/mini-game-reviews_gris.jpg'>
</figure>

## Heroes of the Storm

The MOBA genre is one that I have always liked more in theory than in practice. The original Defense of the Ancients was a regular entry on my list of custom Warcraft 3 maps, and Heroes of Newerth was a favorite at some old-school LAN parties I went to, but the genre has a number of gameplay mechanics that I find more tedious than fun. Last-hitting is the worst of these, but also the concept of carry characters that require protection early in the game to turn around and act as uber strong destroyers late in the game.

Where Heroes of the Storm's strength lies is in doing away with the MOBA mechanics that I dislike. There is no last-hitting mechanic, and all characters on a team level equally. Characters are not perfectly balanced against each other at all levels of play, but none need to be protected while they bloom into something that can hold their own. With mechanics that promote faster gameplay, Heroes of the Storm has a heavy emphasis on group fights. While I am not an expert on Dota 2, the matches I have watched had nowhere near the amounts of up-close fighting as when I play Heroes of the Storm.

In typical Blizzard fashion, Heroes of the Storm is a visual and aural treat. The graphics are in the typical art-style of Blizzard games, but with a higher level of detail than any of their other games. It looks beautiful in action, and native support for 21:9 and higher framerates allows it to feel more modern than some of their other games. The music includes orchestral rock versions of songs from all of their games. They're great remixes, and all fit together cohesively.

One of the negative aspects of development that Blizzard has with World of Warcraft also applies to Heroes of the Storm: endless and needles changes to balance. World of Warcraft famously revamps most classes every expansion, and Heroes of the Storm does the same with its characters. Every patch makes fundamental changes to how one or two characters play, changing their abilities and altering their skills. This included a larger change to the game by the introduction of lootboxes, the addictive gambling mechanic that Blizzard has been in love with since Overwatch's launch.

Out of all of the MOBA games available, Heroes of the Storm resonated with me the most. I enjoyed my time playing it, but with Blizzard wrapping down support for the game, as well as major overhauls to some of my favorite characters, I have little desire to ever play the game again. With the MOBA genre starting to lose favor to the battle royale genre, I do not see myself investing heavily in any other MOBA games after having played Heroes of the Storm.

<figure>
    <img src='/images/mini-game-reviews_heroes-of-the-storm.jpg'>
</figure>

## Quake Champions

The Quake series has always been one of my favorite series of games. Quake 1 and 2 are masterful singleplayer experiences, and Quake 3: Arena is the first PC game that I upgraded my graphics card for. With this background, I was looking forward to playing Quake Champions, despite some reticence over the decision for heroes to have timed special abilities.

The game hits the mark when it comes to fast-paced deathmatch. Quake 3 is one of the faster deathmatch games out there, and Quake Champions is close to it in speed. It feels like a proper Quake game with gorgeous gothic graphics and a wonderful industrial soundtrack by Chris Vrenna and Andrew Hulshult, while still modernizing with new mechanics such as character customization. Despite the hardcore competitive nature of the game, the overall community is positive. There was little trashtalk and immaturity during my time playing it.

Quake Champions is not without flaws. The game's art direction is perfect for a Quake game, but the engine does not hold up to the technical brilliance of previous entries. Performance slow downs and netcode hiccups are a regular occurrence. It is the first Quake game to not use an Id Tech engine, and it shows. Despite Doom having support for Vulkan, the developers have stated that Vulkan will not be coming to Quake Champions. It is also hard to get past the issue of lootboxes in the game. There is an option to purchase the game with all current and future characters unlocked, but all of the customization options come from loot boxes. These can be earned easily while playing, but between duplicates and the sheer amount of items available, it is likely specific looks for characters will need to be purchased for real cash. Unlock Overwatch and other similar games, individual items can not be unlocked with an in-game currency.

Unseating Quake 3: Arena as the king of arena shooters is a difficult task, and Quake Champions does not do that. Still, it is a solid entry into the Quake series and I am happy to have spent time playing it. As of this writing, the game is still in beta, and features are added regularly. The one feature that I miss from the game that will hopefully be added in the future is a Capture The Flag mode. This was my favorite mode to play in Quake 3, and I would love to play it again here.

<figure>
    <img src='/images/mini-game-reviews_quake-champions.jpg'>
</figure>

## Resident Evil 2 (2019 Remake)

Up until this game, the Resident Evil series is one that I have not given a serious try. I briefly played the original Resident Evil 2 back in high school, but I did not get very far with. The remake was not on my radar until I tried the demo due to the rave reviews it was receiving. I was hooked instantly. Since I do not have much experience with the original game, I can only judge it based on what I am experiencing as a new player.

One of my favorite aspects of the game is that the overall area to explore is small. Once all of the doors are unlocked, it takes about a minute to run from one spot to any other spot there, but the journey to unlock it all is fun. It's a maze of puzzles and locked doors to get through, while also avoiding the endless stream of enemies. It is also rich with small details that are a joy to find. Many other games being created these days include large expansive worlds that are not filled with details. It is expected that a person will just run as fast as possible to their destination and not worry about the journey there. Resident Evil 2 takes the opposite approach, with most of the maps have something to discover in every nook and cranny.

Resident Evil 2 does a great job of making the environment feel dangerous. Areas without lights are truly dark, with your flashlight being the only way you can see. Binaural sound plays a huge role in hinting at where danger is about to pop out at you. This is especially true when Mr. X is stalking you through the police station. Listening to the direction his footsteps are coming from helps to avoid him.

The best part about this remake is that a lot of care was given to every detail of the game. Everything about it felt great, and I had a blast playing through both playthroughs of the game. While I do not feel the need to try and get every unlock, I enjoyed my time immensely and recommend anyone into horror and/or survival to give it a go.

<figure>
    <img src='/images/mini-game-reviews_resident-evil-2-2019.jpg'>
</figure>

## Subnautica

This is a game that surpassed my expectations in every way possible. Before Subnautica, the closest thing to a survival game that I played was Metal Gear Solid 3, which was more stealth-action with some light survival elements. The idea of having to "waste time" gathering resources to eat and heal took away from the action, which was more to my liking. The main reason I decided to give Subnautica a chance at all was due to Unknown Worlds Entertainment creating it. They created Natural Selection, one of my all-time favorite online games, so knowing the quality they are capable of pushed me to purchase the game and give it a shot.

What is most striking about the game to me is the art direction of it. Subnautica is a gorgeous game. I often found myself just idling about, enjoying the scenery. Many times I would sit on the top of my escape pod and just watch the sun and then rise again. All of the underwater caves have a unique feel to them that reward exploration. The abandoned bases that you come across are eerie and really feel like something that was built by another person. Sounds plays an important part in the game. As would be expected for a game focused on underwater locales, there are many areas that are nearly pitch black, requiring the use of sound for successfully navigate or to warn of incoming danger. The music adds to the overall ambiance of the game, with a soundtrack that is enjoyable to listen to both in and out of the game.

Subnautica does a great job of giving hints of the way forward without ever forcing you to do so. The world is open to exploration at the player's leisure. The one catch to this is that resources in the game do no respawn. A part of the ocean can be over-fished, or mineral resources tapped completely. This pushes the player to try and live more sustainably, or to go to further depths to explore. There are a lot of options available, all equally right in how you want to approach accomplishing your goal.

Unknown Worlds Entertainment created an amazing experience in this game. By the end, I was happy to have explored the game so thoroughly, but sad that it was ending. I know it will be hard game to go back and play again, as much of the mystery will be gone. But the memories I gained through my first playthrough are still vivid in my mind, even months after playing. No other game has had exploration that felt as rewarding as this one, nor created a world that will stick in my mind for a long time to come.

<figure>
    <img src='/images/mini-game-reviews_subnautica.jpg'>
</figure>

## Unreal

There are three first-person shooters from the 1990s that I consider essential: Doom, Half-Life, and Unreal. All three influenced the genre in ways, while each being their own unique game.

Of the three, Unreal is my favorite to go back and revisit. Whereas the other two are more action-packed, Unreal is slower, with a stronger sense of isolation. Within the game, you are a prisoner stranded on a foreign planet. The sense of being out of place is strong, as local inhabitants react to your presence in both friendly and cautious ways. Some of the world's languages can be translated by your computer, but other parts of the world the player has to piece together to understand.

Unreal is one of the first games where lighting felt like an important facet of the world. The world has a realistic variance in lighting levels, including some sections where the only way to see is by using a hand-held torch. Unreal is not the first game to have multiple colors of lighting, but it uses it more effective than any before it. By modern standards, Unreal does have a soft quality to it, but the art direction still provides a strong sense of wonder to the whole world. Levels are large and open, with a lot of minor details present that help you feel like you are in a world, not just a game.

The music of Unreal stands in stark contrast to many other shooters. Alexander Brandon and Michiel van den Bos created a more ambient soundtrack that melds into the world. It changes tempo to match the mood of the gameplay, but never distracts from what is going on. The soundtrack is great to listen to outside of the game, as there are a lot of interesting textures used to create the music.

Doom and Half-Life get a lot of the glory for 1990s first-person shooters, but Unreal is the one that I have put the most time into. Every year or two, I go back and play through it again, and enjoy it just as much as I always have. Few other games match the mood set by Unreal, which is why I do not think I will ever grow tired of it.

<figure>
    <img src='/images/mini-game-reviews_unreal.jpg'>
</figure>

## What Remains of Edith Finch

What Remains of Edith Finch is the best narrative walking simulator that I have played.

The game is structured as a series of short stories. Each one is about the life and fate of a member of the Finch family. Some of the stories are told in a straight-forward manner, but some are told in a more unique manner. For example, Barbara Finches story is told like an old 1950s horror comic, with the camera moving between comic panes. This is fitting for her character, as she was renowned for screaming in a horror movie, and her death involved an unsolved home invasion, making it ripe for mimicking the style of a *Tales from the Crypt*-styled comic.

What separates this game from others is its visual acumen. The game is the most visually appealing walking simulator that I have played, with a wide range of graphical styles present. From the realistic visuals of the house, to the comic styles of Barbara's story, to the imaginary dream world of Lewis' story, there is much to enjoy in this department. Additionally, Jeff Russo does an amazing job with creating a soundtrack that fits the overall theme of the stories being told.

Like most games in this genre, it is short. The whole game can be experienced in about 2 hours total, and it is best played through in one sitting.

<figure>
    <img src='/images/mini-game-reviews_what-remains-of-edith-finch.jpg'>
</figure>
