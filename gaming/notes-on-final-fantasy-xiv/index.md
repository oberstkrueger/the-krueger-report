---
category: gaming
created: 2017.07.24:1700
title: Notes On Final Fantasy XIV
type: page
updated: 2019.05.29:1145
---

## Trials

### Akh Afah Ampitheatre (Hard)

1. Keep Shiva facing away from group due to cleaving attack.
2. Dreams of Ice: Debuff, increases damage taken.
3. Icicle Impact: Avoid AEs in small patches at the edge of the arena.
4. Hailstorm: Targeted AE, move away from other group members.
5. Absolute Zero: Raid-wide AE.
6. Phase 2: Kill adds.
7. Phase 3: Do not touch edge of arena.
8. Phase 3: Permafrost: Do not move.
9. Phase 3: Icicle Impacts: Move to last AE patch to spawn, and then move out before it explodes.

### Battle on the Big Bridge

1. Focus damage on Enkidu.
2. Pradamante: Move away from other players.
3. Grovel: Point-blank AE.
4. Bomb: Run away from this..
5. Giga Jump: Move away from other players.

### The Bowl of Embers (Normal)

1. When Infernal Nail spawns, switch damage to it.
2. Radiant Plume: phase 3 AEs, first spawn in the center, and then around the outside.

### The Bowl of Embers (Hard)

1. When Ifrit leaps into the air, watch where he and his clones land. Do not stand in his path.
2. Radiant Plume: first spawn around outer edge, and then in the center.
3. Phase 2: 4 Infernal Nails spawn. Change targets to this and kill.
4. Phase 3: Radiant Plume affects entire arena except around Ifrit.

### The Bowl of Embers (Extreme)

1. Pull to 1 o'clock position on map.
2. Suppuration: lowers HP, tank swap at 3 stacks
3. Infernal Nails: change targets and kill one at a time.
4. Crimson Cyclone: 4 Ifrit clones spawn. Run near right-most spawn, wait for it to rush, then follow their positions as they rush.
5. Phase 1 Radiant Plumes: spawns at 1 o'clock, then 7 o'clock
6. Phase 2 Radiant Plumes: spawns at 2 o'clock, then 8 o'clock
7. Phase 3 Radiant Plumes: spawns at 3 o'clock, then 9 o'clock

### Cape Westwind

1. MT: Tank the main boss.
2. OT: Pick up adds.
3. Avoid AE that drops on boss.

### Castrum Fluminis

1. Torment Unto Death: Frontal cone attack.
2. Zashiki-Asobi: Spawns fans around arena that explode in order they spawn.
3. Steel of the Underworld: Frontal cone attack
4. Reprimand: Raid-wide AE.
5. Midnight Haze: Kill adds.
6. Lead of the Underworld: Line-up to share damage attack.
7. Nightbloom: Raid-wide AE.
8. Phase 2: Kill adds.
9. Phase 3: Selenomancy: Switch sides when debuff hits 3 stacks.
10. Phase 3: Lunacy: Stacking shared damage
11. Phase 3: Dark Blade & Bright Blade: Platform-wide AE.
12. Phase 3: Dance of the Dead: Raid-wide AE.

### Emanation (Normal)

1. Gather Vril bubbles and use Duty Action once it is available.
2. Hand of Grace: Move these to opposite ends of the arena. Avoid AE.
3. Hand of Beauty: Move to the back or side of the arena, near each other.
4. Divine Denial: Use Vril, and stand near Lakshmi.
5. Be ready to stack on the Yellow Arrow.
6. Path of Light: Targets OT. Cleave attack, point away from the group.

### The Final Steps of Faith

1. Phase 1: Face Nidhogg away from party.
2. Phase 1: When adds spawn, MT grabs one of the adds that isn't the Shadow Dragon, and OT grabs the other. Face both away from party.
3. Phase 2: Face Nidhogg away from party.
4. Phase 2: If marked, move towards the walls spread out. Will follow up with line attacks towards the center of arena.
5. Phase 3: Same as Phase 1, with additional following mechanics.
6. Phase 3 Aka Morn: Yellow arrow stack attack with multiple hits. Stay stacked until all have gone off.
7. Phase 3 Fireballs: Explode across X and Y axis.

### Hell's Kier (Normal)

1. Cremate: Tank buster.
2. Eternal Flame: Kill adds in center of the arena.
3. Endless Summer: Frontal cone AE.
4. Phoenix Down: Kill middle plume and stand near it. Any adds that spawn, kill.
5. Phase 2: Each player stands in their own circle. Face direction of arrow in circle.
6. Ruthless Refrain: Knockback. Stand near center.
7. Phantom Flurry: Multi-hit tank buster, followed by frontal half-arena AE.
8. Phoenix Says: Watch phoenix as it circles arena. Do not stand in colored section that corresponds to what the phoenix is flying in front of.
9. Incandescent Interlude: One player stands in each circle.

### The Howling Eye (Normal)

1. Keep Garuda facing away from the center rocks.
2. Slipstream: Frontal cone, move to the side.
3. Mistral Song / Aerial Blast: LOS attack, keep a rock between yourself and Garuda.
4. Razor Plumes: Kill adds.
5. Phase 2 Mistral Song: Move behind Garuda.

### The Howling Eye (Hard)

1. Phase 1: Identical to Normal.
2. Phase 2: Tank in northwest corner, avoiding green AE circles on the ground.
3. Phase 3: Two adds spawn. Kill add with green beam between her and Garuda, and then kill other add.
4. Satin Plume: Kill.
5. Eye of the Storm: Move to the center and do not touch blue areas. Kill two adds.

### The Jade Stoa

1. Storm Pulse: Raid-wide AE
2. Heavenly Strike: Tank-buster
3. State of Shock: Stack up
4. Unrelenting Anguish: Avoid red orbs
5. Phase 2 Hakutei-Tiger: Tank this
6. Phase 2 White Herald: move away from rest of group
7. Phase 3 Roar of Thunder: Do as much damage as possible
8. Phase 4 Dodge orbs
9. Phase 4 Bombogenesis: Spread apart
10. Phase 4 Distant Clap: Stand on top of boss or at edge of arena

### The Navel (Normal)

1. Leap: Stay as far away from the center while still not being inside the red circle.
2. Landslide: Avoid or get knocked off the platform.
3. Granite Gaol: Targets one play and traps them inside a rock. Destroy the rock to prevent player from being knocked off.
4. Titan's Heart: Destroy the heart.
5. Weight of the Land: Targets 2 random players. Move out of AE before it goes off.

### The Navel (Hard)

1. Leap: Stay as far away from the center while still not being inside the red circle.
2. Landslide: Avoid or get knocked off the platform.
3. Bomb Boulders: Explode after set period of time, avoid.
4. Granite Gaol: Targets one play and traps them inside a rock. Destroy the rock to prevent player from being knocked off.
5. Titan's Heart: Destroy the heart.
6. Weight of the Land: Targets 4 random players. Move out of AE before it goes off.

### The Pool of Tribute (Normal)

1. Raven Kaikyo: Avoid these AEs.
2. Brightstorm: Stack on player with Yellow Arrows.
3. MT: Spams Duty Action button.
4. OT: Walk in front of the orbs so they do not explode on the group.
5. Earth and Stone: Follow which stone captures a player as it shuffles. Destroy that stone.

### The Pool of Tribute (Extreme)

1. See above notes for normal mode.
2. Churning: 4 players marked with debuff. Players must not be moving or attacking when it wears off.
3. MT and OT must swap roles during Phase 2 giant sword attack.
4. Stormsplitter: MT and OT swap after it hits.
5. Levin Bolt 1: Affected players jump to other side of platform for the AE.
6. Levin Bolt 2: All players move to other side of arena.

### The Royal Menagerie

- 1. Tidal Wave: Move to edge of arena where Tidal Wave spawns.
- 2. Hypernova: All players stack.
- 3. Akh Morn: MT and OT stack away from the rest of the group.
- 4. Spikecycles: Avoid.
- 5. Earth Break: If targeted, move to side of arena to prevent cone attacks from hitting everyone.
- 6. Hellfire: Move into water puddle.
- 7. Judgement Bolt: Stay outside of water puddles.
- 8. Phase 2: Kill adds.
- 9. Phase 3: Tail Slap: Avoid AE, then kill tail.
- 10. Phase 3: Burning Chains: Move away from chained partner.
- 11. Phase 3: Levin Bolt: Move away from other players.
- 12. Phase 3: Dragonfist: Move away from middle tile.
- 13. Phase 3: Diamond Dust: Do not move.
- 14. Phase 3: Aerieal Blast: Cyclone in center will push players back. Keep running towards it.

### The Striking Tree (Hard)

1. Thunderspark: Point-blank AE.
2. Thunderstorm: Spread out so not overlapping
3. Offtank: Pick up 3 orbs left behind by Thunderstorm.
4. Kill adds.
5. Rolling Thunder: Tethers two players. One must grab 3 orbs to break tether.

### Thornmarch (Hard)

1. Phase 1: Kill all adds.
2. Phase 2 MT: Tank King Moggle Mogl XII.
3. Phase 2 OT: Pull Whiskerwall Kupdi Koop (PLD) and Ruffletuft Kupta Kapa (WAR) away from the group and tank.

In Phase 2, the DPS will kill adds in this order:

1. Pukla Puki the Pomburner (BLM)
2. Furryfoot Kupli Kipp (WHM)
3. Puksi Piko the Shaggysong (BRD)
4. Woolywart Kupqu Kogi (ARC)
5. Pukna Pako the Tailturner (ROG)

### The Whorleater (Hard)

1. One tank on the head of Leviathan, and one on the tail.
2. Do not stand in front of geysers from the water.
3. Kill Gyre Spume quickly.
4. Off-tank picks up any additional adds.
5. Before Tsunami goes off, activate the Elemental Shield.

### The Wreath of Snakes (Normal)

1. Infirm Soul: Tankbuster.
2. Onmyo Circle: PBAE.
3. Dragon Ring: Stand inside melee range.
4. Summon Shiki: Tank adds. Explode on death, so don't kill all at once.
5. Phase 2: Orochi: Knockback into the water.
6. Phase 2: Big Boi Shiki: Avoid arm AEs.
7. Phase 2: Force of Nature: Stand near middle circle but not inside.

## Alliance Raids

### The Orbonne Monastery

1. Mustadio
    1. Arm Shot: Tankbuster.
    2. Left/Right Handgonne: Move to opposite side of boss.
    3. Maintenance: Spawns adds that create line AEs.
    4. Analysis: Face the gap in the market towards the boss.
    5. Leg Shot: Spawns mines on platform. Stay away.
    6. Maintenance 2.0: Respawns adds that AE for their quarter of the arena.
    7. Maintenance 3.0: Combines mechanics of Maintenance 1.0 and 2.0.
2. Agrias
    1. Thunder Slash: Frontal cone tankbuster.
    2. Shieldbearer: Stand in circle, face boss, and then use duty action button.
    3. Cleansing Strike: Targets one group of raid. That group must kill adds.
    4. Swordbearer: Inside Cleaning Strike, same as Shieldbearer but must be used against adds with shield barriers.
    5. Holy Blade Transition: Kill adds. Use Shieldbear during Mortal Blow.
    6. Divine Ruination: Use Shieldbearer to block.
3. The Thunder God
    1. TG Holy Sword: If horizontal, don't stand on platforms being pointed at. If vertical, move close to boss. If horizontal but not pointing at platforms, move to edge of arena.
    2. Shadowblade: Marked player moves away from group. Each alliances effect can not touch another.
    3. Crush Helm: Tankbuster.
    4. Duskblade: 3 players must stand in each circle.
    5. Crush Weapon: Targeted player moves away from other players and then runs from AE.
    6. Coliseum: Kill adds.
    7. Crush Armor: Pass tether between non-tanks.
    8. Crush Accessory: Kill ice adds.
4. Ultima, the High Seraph
    1. Auralight: Line AEs that create walls.
    2. Grand Cross: Explodes in cross patterns.
    3. Uses multiple attacks from previous Ivalice raid bosses.
    4. Phase 2: All 3 previous boss attacks spawn at once.
    5. Phase 2: Ultimate Illusion: Stand inside bubble.
    6. Phase 3: East/West March: Moves all adds and AEs in the named direction.
    7. Phase 3: Redemption: Tankbuster.
    8. Phase 3: Demi-Virgo: Stand underneath circle spawns.
    9. Phase 4: Maze: Navigate back towards the boss.

## Raids

### Alphascape 1.0

1. Chaotic Dispersion: Tank-buster.
2. Tsunami: Stay in the middle third of the arena.
3. Dynamic Fluid: Stack in the middle.
4. Knock Down: Move to edge of arenas.
5. Fiendish Orbs: Both tanks intercept orbs to take damage.
6. Knock Back: Stand along edge of platform to avoid being knocked into damaged area. Two pairs of two players marked with stack up shared damage, must separate into two even groups.
7. Blaze: Stay in middle circle of platform.
8. Cyclone: Tornado in center will occasionally push back towards edge of arena. Stay near tornado.

### Alphascape 2.0

1. Circle spin -> circle spin: Point-blank AE.
2. Circle spin -> loop: Donut AE, move on top of him.
3. Two loops: Move to corner of arena.
4. Earth Shakers: Move away from other players.
5. Akh Morn: Stack up damage, multiple hits.
6. Thunderstorm: Move away from other players.
7. Blizzard Markers: Move away from center of arena.
8. Cauterize: AE damage across arena.
9. Immortal Keys: Kill these quickly.
10. Frost Breath: Frontal cone.
11. Tail End: Tank buster.
12. Exaflares: Move across platform.
13. Akh Rahl: Move away from damage.

### Alphascape 3.0

1. Mustard Bomb: Tank-buster.
2. Ballistic Impact: Leaves behind flame puddles.
3. Larboard: Cone attack on left side of boss.
4. Starboard: Cone attack on right side of boss.
5. Peripheral Synthesis: Use Duty Action on adds.
6. Program Loop: Stay in donut shape. Kill add.
7. Delta Attack: Spam Duty Action key.
8. Blaster: Off-tank intercepts and moves away from group.

### Alphascape 4.0

1. Solar Ray: Tank-buster
2. Program Alpha: Stack up shared damage.
3. Subject Simulation F: Move close to Omega F to survive pushback.
4. Optimized Blade Dance: Tank-buster, use multiple cooldowns.
5. Subject Simulation M: Move away to avoid point-blank AE.
6. Omegoo: Move both towards edge and apart from each other.
7. Shield + Shower: Kill Omega-M and then Omega-F.
8. Firewall: Debuff that prevents damage to one of the Omega forms.
9. Keep Omega-F and Omega-M separated.

### Deltascape V1.0 (Normal)

1. Move away from fire orbs, as they explode for damage and debuff.
2. Breathwing: Knocks back players and fire orbs. Pre-position close to the boss.
3. Twin Bolt: hits the MT and whoever is physically closest to them. OT should stand on top of MT before its casting concludes.
4. Levinbolt: Move away from other players.
5. The Classical Elements: multiple above abilities at once.
6. Charybdid: Brings all players to low HP. Stand near healer to be AE healed up.

### Deltascape V2.0 (Normal)
1. Earthquake: Use Duty Action to levitate above damage.
2. Evil Sphere: Tank-buster.
3. Aetherial Rift: Use Duty Action to rise out of the sinking death.
4. Gravitational Explosion: Yellow arrow stack.
5. Maniacal Probe: After Epicenter is cast, move away from any raised tentacle with a blue eye above it.
6. Blue Spheres: Use Duty Action to levitate above damage.
7. Yellow Spheres: Make sure you're on the ground to avoid damage.
8. Demon Eye: Gaze attack.
9. -1000GS: Stand on the side where no blue or yellow spheres are.

### Deltascape V3.0 (Normal)

1. Ribbit: Frontal cone, turns player into frog.
2. Spellblade Holy: Yellow arrow stack on one player, AE damage on two other marked players.
3. Place Dark Token: Adds spawn that have PBAE attacks.
4. The Playing Field: Four tiles spawn in the middle of the room, each with a different class role icon. Stand on the icon with the same role as your character.
5. Panel Swap: Stay standing on light blue tiles at all times to avoid The Queen's Waltz.
6. Place Token: Spawns yellow dragon, OT should tank facing away from party.
7. Mindjack: Forces players to run in specified directions.
8. Aetherial Tear: Sand maze.
9. Phase 3: Some of the floor tiles will be of frogs. Get hit by Ribbit to avoid damage from these attacks.

### Deltascape V4.0 (Normal)

1. Void: When Exdeath is drawing energy from the black void in the back of the arena, attacks are altered as listed below.
2. Blizzard III: AE.
3. Blizzard III + Void: Move around as the cast finishes.
4. Fire III: Marks players for PBAE damage.
5. Fire III + Void: Do not move or attack while debuff is present.
6. Thunder III: Tankbuster and a debuff. When two stacks of debuff are present, MT and OT swap roles.
7. Thunder III + Void: Large AE + debuff. Move away.
8. Vacuum Wave: Knocks players back
9. Holy: Yellow arrow stack.
10. The Decisive Battle: Run behind the creepy face to avoid frontal cone debuff.
11. Flare: All marked players move away.
12. Meteor: High unavoidable damage.
13. Black Hole: Places black holes around arena that debuts.

### Sigmascape V1.0 (Normal)

1. Spooky Ghosts: Avoid these AEs.
2. Head On: Move to the rear end of the platform.
3. Spotlight: If marked with this, move the trailing light towards a ghost without touching the ghost.
4. Diabolical Whistle: Ghost followed marked player. Bring it into a Spotlight.
5. Diabolic Headlamp: Line up between boss and marked player.
6. Doomstrike: Tank buster.
7. Diabolical Wind: Move away from other players.
8. Diabolical Light: Move to the back of the platform.
9. Phase 2: Run into a ghost before dying.

### Sigmascape V2.0 (Normal)

1. Demonic Shear: Tank buster.
2. Prey: Move away from the rest of the group.
3. Possession on fire painting: Use duty action on the water sketch.
4. Possession on earth painting: Use duty action on the air force sketch.
5. Possession on water painting: One player uses their duty action on boulder sketch, then everyone hides behind the boulder.
6. Possession on wind painting: One player uses duty action on typhon sketch, then pushes adds away.
7. Phase 2: Uses possession on 2 paintings at once.

### Sigmascape V3.0 (Normal)

1. Watch the screens at the north end of the arena. They telegraph what abilities it will use next.
2. Arm and Hammer: Tank buster.
3. All of the following programs spawn an add that must be tanked by the OT.
4. Ultros Program: Avoid AEs and tenacles.
5. Dadaluma Program: Stand in a green circle.
6. Air Force Program: Avoid AEs and missiles.
7. Bibliotaph Program: Groups of 3 must stand in each circle.

### Sigmascape V4.0 (Normal)

1. If any of the following are marked with question marks, do the opposite of the normal effect.
2. Hyperdrive: Main tank buster.
3. Blizzard Blitz: Move into melee range.
4. Flagrant Fire: All marked players move away from other players.
5. Thrumming Thunder: Columns of AEs throughout arena.
6. Ultima Upsurge: Raid-wide AE.
7. Timely Teleport: Moves to other side of arena. Stand to side or behind to avoid damage.
8. Aero Assault: AE knock back.
9. Phase 2: Graven Image: Move toward orb to avoid being knocked off platform.
10. Phase 3: Graven Image: Look away from the tower.
