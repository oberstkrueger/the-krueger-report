---
category: appendices
created: 2019.05.16:1535
title: Bands I Have Listened To
type: page
updated: 2019.08.28:1455
---

This page is split into two sections. The first section lists which bands I have listened to the complete discography for. The second section are bands that I have sampled, usually by listening to a [greatest hits album](https://en.wikipedia.org/wiki/Greatest_hits_album) or an essentials playlist on Apple Music.

Note: As of now, this is an incomplete list. I am still working through a list of artists I know I have listened to, making sure that I have listened to all that is available by them before moving on to the next.

## Complete Discography

- AFI
- After Forever
- Amaranthe
- Amberian Dawn
- And Then She Came
- Angtoria
- Apocalyptica
- Apoptygma Berzerk
- Archive
- Arven
- Atargatis
- Autumn
- Ayria
- Babymetal
- Chumbawamba
- Daft Punk
- Dead Can Dance
- Delain
- Deltron 3030
- Dream Theater
- Elis
- Eluveitie
- Emigrate
- Emilie Autumn
- Epica
- Eyes of Eden
- Fiona Apple
- For My Pain...
- Gorillaz
- HolyHell
- How To Destroy Angels
- Hydria
- Icon of Coil
- Indica
- Jeff Buckley
- Juno Reactor
- Kerion
- Led Zeppelin
- Limp Bizkit
- Lindemann
- Linkin Park
- Lisa Gerrard
- Liv Moon
- Mandragora Scream
- Manntra
- MaYaN
- Metallica
- Miktek
- Mono Inc.
- Muse
- Nightwish
- Nine Inch Nails
- Northern Kings
- Omega Lithium
- Orgy
- Perturbator
- Portishead
- Rammstein
- Slipknot
- Static-X
- System of a Down
- t.A.T.u.
- Tacere
- Tenacious D
- The Birthday Massacre
- The Fatman and Team Fat
- The Grim Faeries
- Theatres des Vampires
- Tool
- Trail of Tears
- Turilli/Lione Rhapsody
- Violet Sun
- Voices of Destiny
- We Are The Fallen
- Within Temptation
- Xandria
- Zombie Girl

## Explored

- Bad Religion
- Beastie Boys
- Beck
- Blink 182
- The Cranberries
- Diary of Dreams
- Dr. Dre
- Green Day
- Judas Priest
- Mastadon
- Megadeth
- Mos Def
- Nirvana
- R.E.M.
- Stone Temple Pilots
- Sum 41
- Zola Jesus
- 311
