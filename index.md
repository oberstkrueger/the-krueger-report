---
category: root
created: 2016.11.05:1300
title: The Krueger Report
type: index
updated: 2019.05.16:1535
---

## Introduction

- [Hello, world!](/introduction/hello-world)
- [Updates](/introduction/updates)
- [About The Author](/introduction/about-the-author)
- [Tools Used To Create This](/introduction/tools-used-to-create-this)

## Gaming

- [Mini Game Reviews](/gaming/mini-game-reviews)
- [Asheron's Call](/gaming/asherons-call)
- [Disabling Gaming Platform's Social Features](/gaming/disabling-gaming-platforms-social-features)
- [The Games of Tomorrow Corporation](/gaming/the-games-of-tomorrow-corporation)
- [Notes On Final Fantasy XIV](/gaming/notes-on-final-fantasy-xiv)

## Health

- [Iced Coffee](/health/iced-coffee)
- [Vitamin B<sub>12</sub>](/health/vitamin-b12)
- [Tracking Sleep](/health/tracking-sleep)

## Life

- [Selecting Movies](/life/selecting-movies)
- [Traveling Minimally](/life/traveling-minimally)
- [Years In Review](/life/years-in-review)

## Automation

- [Why Automate?](/automation/why-automate)
- [Automating Podcasts](/automation/automating-podcasts)

## Programming

- [base-css](/programming/base-css)
- [Exit Status Codes](/programming/exit-status-codes)
- [Helpful Algorithms](/programming/helpful-algorithms)
- [Project Euler In Swift](/programming/project-euler-in-swift)
- [Scripting With Swift](/programming/scripting-with-swift)
- [Updating Sublime Text](/programming/updating-sublime-text)

## Technology

- [Software Tools](/technology/software-tools)
- [CLI Tools](/technology/cli-tools)
- [Simplifying the Smartphone](/technology/simplifying-the-smartphone)
- [Backups](/technology/backups)
- [DuckDuckGo](/technology/duckduckgo)
- [Logging Off Facebook](/technology/logging-off-facebook)

## All About The Mac

- [Setting Up Homebrew](/all-about-the-mac/setting-up-homebrew)

## Music

- [Rebooting iTunes](/music/rebooting-itunes)
- [Work Music](/music/work-music)

## Trips

- [Walt Disney World - 2016](/trips/walt-disney-world-2016)
- [Solar Eclipse - 2017](/trips/solar-eclipse-2017)

## Notes On Books

- [Bored and Brilliant](/notes-on-books/bored-and-brilliant)
- [Crash Override](/notes-on-books/crash-override)
- [The Half-Life of Facts](/notes-on-books/the-half-life-of-facts)
- [Moonwalking With Einstein](/notes-on-books/moonwalking-with-einstein)
- [Resilient Web Design](/notes-on-books/resilient-web-design)

## Appendices

- [Bands I Have Listened To](/appendices/bands-i-have-listened-to)
- [Books I Have Read](/appendices/books-i-have-read)
- [Games I Have Played](/appendices/games-i-have-played)
- [Movies I Have Watched](/appendices/movies-i-have-watched)
- [Podcasts I Have Listened To](/appendices/podcasts-i-have-listened-to)
- [Random Notes](/appendices/random-notes)
