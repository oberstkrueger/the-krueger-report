---
category: introduction
created: 2016.02.10:0606
title: About The Author
type: page
updated: 2019.04.01:1255
---

## Current Computing Devices

### Gaming PC

CPU: [Intel Core i7-6700](http://ark.intel.com/products/88196/Intel-Core-i7-6700-Processor-8M-Cache-up-to-4_00-GHz)<br>
GPU: [Nvidia GeForce GTX 1060](https://www.nvidia.com/en-us/geforce/products/10series/geforce-gtx-1060/)<br>
RAM: [ADATA AX4U2133W4G13-DRZ 8GB DDR4-2133](http://www.adata.com/en/xpg-dram/orderinfo/305)<br>
SSD: [Samsung 850 EVO 120GB](http://www.samsung.com/us/computing/memory-storage/solid-state-drives/ssd-850-evo-2-5-sata-iii-120gb-mz-75e120b-am/) & [500GB Samsung 850 EVO SSD](http://www.samsung.com/us/computing/memory-storage/solid-state-drives/ssd-850-evo-2-5-sata-iii-500gb-mz-75e500b-am/)<br>
Peripherals: [LG 25UM58-P](http://www.lg.com/us/monitors/lg-25UM58-P-ultrawide-monitor), [Steam Controller](http://store.steampowered.com/app/353370/)

Geekbench CPU: [4937 / 16927](https://browser.geekbench.com/v4/cpu/12004240)<br>
Geekbench GPU: [140031](https://browser.geekbench.com/v4/compute/3645486)

### iPad

CPU: [Apple A9, 1.8 GHz](https://en.wikipedia.org/wiki/Apple_A9)<br>
GPU: [PowerVR GT7600](https://en.wikipedia.org/wiki/PowerVR#Series7XT_.28Rogue.29)<br>
RAM: 2048 MB<br>
SSD: 32 GB

Geekbench CPU: [2560 / 4465](https://browser.geekbench.com/v4/cpu/12003927)<br>
Geekbench GPU: [10681](https://browser.geekbench.com/v4/compute/3645112)

### iPhone 6 Plus

CPU: [Apple A8, 1.4 GHz](https://en.wikipedia.org/wiki/Apple_A8)<br>
GPU: [PowerVR Series 6 GX6450](https://en.wikipedia.org/wiki/PowerVR#Series6XT_.28Rogue.29)<br>
RAM: 1024 MB<br>
SSD: 64 GB

Geekbench CPU: [1593 / 2737](https://browser.geekbench.com/v4/cpu/12003949)<br>
Geekbench GPU: [4604](https://browser.geekbench.com/v4/compute/3645142)

### Mac Mini

CPU: [Intel Core i7-8700B](https://ark.intel.com/products/134905/Intel-Core-i7-8700B-Processor-12M-Cache-up-to-4-60-GHz-)<br>
GPU: [Intel UHD Graphics 630](https://en.wikipedia.org/wiki/Intel_Graphics_Technology#Kaby_Lake_Refresh_/_Coffee_Lake)<br>
RAM: 16 GB 2667 MHz DDR4<br>
SSD: [512GB Apple SSD]<br>
Peripherals: [LG 25UM58-P](http://www.lg.com/us/monitors/lg-25UM58-P-ultrawide-monitor), [Ducky MIYA Pro Mac](https://mechanicalkeyboards.com/shop/index.php?l=product_detail&p=4285), [Magic Trackpad](https://en.wikipedia.org/wiki/Magic_Trackpad)

Geekbench CPU: [5974 / 26510](https://browser.geekbench.com/v4/cpu/12004493)<br>
Geekbench GPU: [24774](https://browser.geekbench.com/v4/compute/3645324)

## Current Workspaces

<figure>
	<img src='/images/about-the-author_macos.jpg'>
	<figcaption>macOS</figcaption>
</figure>

<figure>
	<img src='/images/about-the-author_ipad.jpg'>
	<figcaption>iPad</figcaption>
</figure>

<figure class='half'>
	<img src='/images/about-the-author_iphone.jpg'>
	<figcaption>iPhone</figcaption>
</figure>

## Contact

I can be contacted on Matrix: @oberstkrueger:matrix.org
