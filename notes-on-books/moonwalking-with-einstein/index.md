---
category: notes-on-books
created: 2018.01.02:1130
title: Moonwalking With Einstein
type: page
updated: 2018.01.02:1130
---

**Title**: Moonwalking with Einstein: The Art and Science of Remembering Everything<br>
**Author**: Joshua Foer<br>
**Author's Background**: Journalist; winner of 2006 U.S.A. Memory Championship

### Notes
- Most memory competition champions use a technique known as "memory palace" to complete their challenges. These draw on memorization techniques dating back 2,500 years, and were used heavily up through the Middle Ages when the printing press removed the need for such heavy memorization.
- In London, a common sight is of people driving scooters around the city, maps attached to the front. These individuals are practicing for "the Knowledge", an exam to become a cabdriver where the person must prove extensive and complete knowledge of the cities roads. Eleanor Maguire of University College London performed fMRI scans of some of these drivers in and discovered that their posterior hippocampus was 7% larger than normal. The massive amounts of spatial knowledge they held had physically increased a portion of their brain.
- Eleanor Maguire, Elizabeth Valentine, and John Wilding teamed up and did a similar study on memory competition winners. Their brain structure matched that of a control group, but the portions of the brain activated during memorization was different. The parts of the brain associated with visual memory and spatial navigation were activated, which includes the posterior hippocampus.
- The "Baker/baker paradox" is when two groups of people are shown the same picture of a man and told two different things about him. The first group is told that his surname is Baker. The second group is told that he is a baker. Days later, the second group has an easier time remembering the accompanying word with the picture than the first group.
- In 1956, George Miller of Harvard wrote about how most people have a working memory of 7 items, plus or minus 1 to 2. This means that anything beyond that number of things in working memory must be either discarded or find a way to move it to long-term memory first.
- Chunking is the act of taking individual pieces of information and grouping them together. For example, the numbers 1, 2, 3, 4, 5, 6 can be chunked together as 123 and 456.
- When magnetoencephalography is used to measure activity in the brains of chess masters, most activity is centered around the frontal and parietal cortices, the areas of the brain associated with long-term memory. Novice chess players are more likely to use the medial temporal lobes, the areas associated with encoding newly gained information.
- Anterograde amnesia: when one can not form new memories.
- Retrograde amnesia: when one can not recall old memories.
- Michel Siffre secluded himself inside a cave for 2 months and did nothing but eat and sleep. During this time, his lost the ability to form new memories and to be able to tell time apart. The lack of novel activities affected his mental faculties immensely.
- The hippocampus is the main part of the brain responsible for forming memories.
- Two types of memories: declarative/explicit and non-declarative/implicit. Explicit memories are ones we know we remember: conscious thoughts and facts. Explicit memories are further categorized as semantic memories and episodic memories. Semantic memories are specific facts, whereas episode memories are memories that have a time and place associated with them. Implicit memories are those related to actions, such as riding a bike. Implicit memories are tied to other portions of the brain.
- Motor skill learning takes place in the cerebellum.
- Perceptual learning takes place in the neocortex.
- Habit learning takes place in the basal ganglia.
- Elaborative encoding is when you take mundane facts and translate them into something exciting so that the brain remembers it better.
- The memory palace, or method of loci, is to create a visual space in the mind and place objects within that space. Each object is some form of what you want to remember. This helps encode the memory into a form that the human mind can more easily remember. Memory palaces don't have to be to empty voids, but can be routes along a street or a river or something else.
- Both *The Iliad* and *The Odyssey* were originally passed down orally, which explains some of the repetitive structures used within the stories. Despite this, they are incredibly long works of text.
- Researcher Milman Parry followed bards from Yugoslavia, recording their stories. While the overall structures of the stories stayed the same, small differences emerged in the details of them on each retelling. When asked about this, the bards stated that the stories were always the same. Parry concluded this was due to them not having a concept of verbatim renditions of the story, but of just general poetic tellings. Passages and words that are harder to remember are naturally forgotten by the bards, and only the core of the story remains.
- In the memory contests covered by the author, women used different techniques than men. For memorization of poetry, they would try to understand the emotions of the poem more than the visual techniques used by the men.
- Punctuation was invented in 200 B.C. by the director of the Library of Alexandria, Aristophanes of Byzantium. Before this, all letters just ran together in written texts.
- The Major System, invented by Johann Winkelmann, involves memorizing numbers as words. Numbers are combined and matched to specific consonants, and vowels are interspersed in any manner that the memorizer would like.
- The PAO system associates a different person, action, and object with all numbers from 00 to 99. To memorize any 6 digit number, you memorize the person associated with the first 2 digits, the action associated with the 3rd and 4th digits, and the object associated with the 5th and 6th digits. This allows for every number from 0 to 999,999 to be associated with a unique image. To work best, a PAO system needs to have its imagery chosen anew by each memorizer. The images will correlate to what is memorable to them.
- Many competitive memory champions use noise-canceling earmuffs and blinders to help them focus on their task and block out any and all outside distractions.
- Studies by psychologists Edward Thorndike and Robert S. Woodworth showed that practicing memorization did not show increased gains in the general ability of memorizing texts, just an increased ability to memorize more of what you practiced to memorize.

### Quotes

> People assume that memory decline is a function of being human, and therefore natural,” [Tony Buzan] said. “But that is a logical error, because normal is not necessarily natural. The reason for the monitored decline in human memory performance is because we actually do anti-Olympic training. What we do to the brain is the equivalent of sitting someone down to train for the Olympics and making sure he drinks ten cans of beer a day, smokes fifty cigarettes, drives to work, and maybe does some exercise once a month that’s violent and damaging, and spends the rest of the time watching television. And then we wonder why that person doesn’t do well in the Olympics. That’s what we’ve been doing with memory.

<div></div>

> The externalization of memory not only changed how people think; it also led to a profound shift in the very notion of what it means to be intelligent. Internal memory became devalued. Erudition evolved from possessing information internally to knowing how and where to find it in the labyrinthine world of external memory. It’s a telling statement that pretty much the only place where you’ll find people still training their memories is at the World Memory Championship and the dozen national memory contests held around the globe. What was once a cornerstone of Western culture is now at best a curiosity. But as our culture has transformed from one that was fundamentally based on internal memories to one that is fundamentally based on memories stored outside the brain, what are the implications for ourselves and for our society? What we’ve gained is indisputable. But what have we traded away? What does it mean that we’ve lost our memory?

<div></div>

> Socrates goes on to disparage the idea of passing on his own knowledge through writing, saying it would be "singularly simple-minded to believe that written words can do anything more than remind one of what one already knows." Writing, for Socrates, could never be anything more than a cue for memory—a way of calling to mind information already in one’s head. Socrates feared that writing would lead the culture down a treacherous path toward intellectual and moral decay, because even while the quantity of knowledge available to people might increase, they themselves would come to resemble empty vessels. I wonder if Socrates would have appreciated the flagrant irony: It’s only because his pupils Plato and Xenophon put his disdain for the written word into written words that we have any knowledge of it today.

<div></div>

> What separates experts from the rest of us is that they tend to engage in a very directed, highly focused routine, which Ericsson has labeled "deliberate practice." Having studied the best of the best in many different fields, he has found that top achievers tend to follow the same general pattern of development. They develop strategies for consciously keeping out of the autonomous stage while they practice by doing three things: focusing on their technique, staying goal-oriented, and getting constant and immediate feedback on their performance. In other words, they force themselves to stay in the "cognitive phase."
>
> Amateur musicians, for example, are more likely to spend their practice time playing music, whereas pros are more likely to work through tedious exercises or focus on specific, difficult parts of pieces. The best ice skaters spend more of their practice time trying jumps that they land less often, while lesser skaters work more on jumps they’ve already mastered. Deliberate practice, by its nature, must be hard.

### Further Reading

- Unknown author - *Rhetorica ad Herennium*
- Homer - *The Iliad*
- Homer - *The Odyssey*
