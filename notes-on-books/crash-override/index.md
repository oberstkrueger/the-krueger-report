---
category: notes-on-books
created: 2017.10.21:0615
title: Crash Override
type: page
updated: 2017.10.21:0615
---

**Title**: Crash Override: How Gamergate (Nearly) Destroyed My Life, and How We Can Win the Fight Against Online Hate<br>
**Author**: Zoë Quinn<br>
**Author's Background**: Indie video game developer and programmer

### Notes

- The origins of Gamergate are based on the false story of the author having sex with a reviewer in exchange for a good review of her games. This is where the "ethics in games journalism" mantra that many in Gamergate used as the reason for their crusade.
- Online "trolling" and domestic violence are carried out through similar actions: instilling fear, trying to control other people, and punishment through intimidation.
- Actor Adam Baldwin is the first person to use the term "Gamergate".
- Tips for securing your online presence:
	- Use a different password for every account. Make use of a password manager to assist with this.
	- Use two-factor authentication when possible.
	- Do a search for your name, address, and phone number on all search engines. Use the results to determine what accounts to remove information from or delete.
	- Keep social media private.
	- Do not use real answers to security questions for any account.

### Quotes

> Unfortunately, [...] people participating in online abuse treat it like a game, too, seeing who can do the most damage to a target they see as a dehumanized mass of pixels on a screen, more like a monster in a game to be taken down than an actual human being with thoughts and hopes and weaknesses and moments of brilliance.

<div></div>

> For every harmless community of users into really specific sexual kinks, there is a place like Bareback Exchange, a forum for people who get off on transmitting STDs to as many people as possible, often without consent. For every community of angsty kids who pretend they are secretly vampires, there are seven different forums of white nationalists who sincerely believe that Jewish people are secretly vampires. For every geeky and silly toy collector’s community, there are forums full of dudes collecting upskirt photos of random women and girls who had no idea they were about to become porn.
