---
category: notes-on-books
created: 2018.02.22:0320
title: The Half-Life of Facts
type: page
updated: 2018.02.22:0320
---

**Title**: The Half-Life of Facts: Why Everything We Know Has an Expiration Date<br>
**Author**: Samuel Arbseman<br>
**Author's Background**: PhD in Computational Biology from Cornell, Scientist in Resident at Lux Capital

### Notes

- In 1963, Derek J. de Solla Price published *Little Science, Big Science*. In it, he included information that showed over how long a period of time it took for the following domains to double in size:
	- Entries in a dictionary: 100 years
	- Universities: 50 years
	- Important discoveries: 20 years
	- Scientific journals: 15 years
	- Known chemical compounds: 15 years
	- Membership in scientific institutes: 15 years
	- Known asteroids: 10 years
	- Engineers in the United States: 10 years
- In 1947, Harvey Lehman published a similar list looking at the doubling rate of contributions in specific fields:
	- Medicine and hygiene: 87 years
	- Philosophy: 77 years
	- Mathematics: 63 years
	- Geology: 46 years
	- Entomology: 39 years
	- Chemistry: 35 years
	- Genetics: 32 years
	- Grand opera: 20 years
- Journal articles with higher impact scores are more likely to come from research conducted by a team of scientists than from a single scientist.
- Nobel laureates are listed as the first author for their own journal articles only 26% of the time, compared to less accomplished scientists that appear as first authors 56% of the time. Nobel laureates tend to give junior members of their research first authorship earlier than non-laureates.
- Early scientific experiments required asking the right questions, but could be tested in much simpler ways than modern science. Most of the easier discoveries have been taken care of, with the harder and more computationally difficult questions remaining.
- Ease of discovery goes down by an average percentage each year.
- The last human organ to be discovered was the parathyroid gland. It was discovered in 1880.
- Lazaras taxa: Species thought to be extinct by later discovered to still be alive.
- The logistic curve is the function that shows an exponential growth slowing down as it reaches its carrying capacity. Also known as an S-curve due to its shape.
- S-curves of multiple related technologies can be combined to show how the related technologies grow as a whole, even if individual technologies have reached their carrying capacity.
- Magnetism in iron has increased over the last couple hundred years, as iron is mined and manufactured in a more pure form.
- The actuarial escape velocity is when additional years of lifespan are added at a rate of more than one per year, therefore creating immortality. The term was coined by Aubrey de Grey.
- A hyperbolic growth rate is where the rate of growth continues to increase as the population increases. This eventually leads to an infinite growth rate.
- The first cities to receive the printing press were those with larger German communities who could better understand Gutenberg's work.
- Within the first 50 years after the invention of the printing press, its use spread to only 1/3 of European cities. These cities skewed towards being the larger ones, so that those 1/3 of cities had more than half of Europe's population.
- Errors in texts can be used to track how knowledge spreads. If an error is made in one copy of a text, and later dated copies of the text contain the same error, they most likely originated from that original incorrect copy.
- About 20% of the authors for scientific journals read the original paper before citing it. The rest of the time, they cite it based on how another article cited it.
- When conducting a new study, scientists cite less than 25% of the relevant research to that study, with a bias towards newer research over older research.

### Quotes

> Imagine a small group of randomly chosen people stranded on a desert island. Not only would they have just a small subset of the knowledge necessary to re-create modern civilization—assuming Gilligan’s professor wasn’t included—but only a tiny fraction of the required skills could be done by each person. Much like the economic concept of division of labor, even if we each have two or three skills, to perform all of them adeptly, and also pass them along to our descendants, is a difficult proposition. The maintenance and creation of cultural knowledge are much more easily done with large groups of people; each person can specialize and be responsible for a smaller area of knowledge.

<div></div>

> Bad information can spread fast. And a first-mover advantage in information often has a pernicious effect. Whatever fact first appears in print, whether true or not, is very difficult to dislodge.
