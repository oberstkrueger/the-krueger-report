---
category: notes-on-books
created: 2017.11.12:1650
title: Bored and Brilliant
type: page
updated: 2017.11.12:1650
---

**Title**: Bored and Brilliant: How Spacing Out Can Unlock Your Most Productive and Creative Self<br>
**Author**: Manoush Zomorodi<br>
**Author's Background**: Host of Note To Self podcast

### Notes

- When the mind is allowed to wander, it activates the default mode network. This is the parts of the brain that reflect on a person’s self, others around them, and creates new ideas about possible futures.
- When the mind is busy with a task, the executive attention network is activated. This is the parts of the brain that let you focus on the world around you and with tasks at hand.
- 7 day challenge:
	- 1. **Observe yourself**: Keep a log of all digital device usage, mobile or otherwise. Do not change any behavior, just write it down in any form.
	- 2. **Keep your devices out of reach while in motion**: Keep your phone in your bag or pocket while moving around. No walking and texting.
	- 3. **Photo-free day**: Do not take photos of anything.
	- 4. **Delete that app**: Find your most-used application and remove it from your mobile devices.
	- 5. **Take a fakecation**: Set up a period of time to ignore all communication and social media. Set an auto-away message so that people know you will be unreachable, and stick to not responding.
	- 6. **Observe something else**: Spend some time watching other things.
	- 7. **The Bored and Brilliant Challenge**: First, identify a problem in your life that you wish to solve. Second, spend 30 minutes doing something incredibly boring with zero distractions. Third, spend time writing out solutions to the task decided upon in the first step.
- Dr. Sandi Mann created an experiment with two phases. The first phase had participants copy phone numbers out of a phone book for 20 minutes, and then were asked to come up with as many possible uses for two cups as they could. The second phase had participants speak aloud phone numbers out of a phone book for 20 minutes, and then asked the same question about two cups. Participants of the second phase came up with more ideas both in creativity and number than those who participated in the first phase.
- According to fMRI studies, the brain is only 5% less active when using the default mode network when compared to the executive attention network.
- There are three types of mind wandering:
	- **Poor attention control**: When people have difficulty concentrating, where even daydreams skip around a lot and are unproductive.
	- **Guilty-dysphoric**: When the mind wanders to negative thoughts and focuses on them. This is both unproductive and can lead to other negative actions to try and quell the negative thoughts.
	- **Positive-constructive**: The good kind of daydreaming, the opposite of guilty-dysphoric.
- Research by Tim Wilson and Dan Gilbert placed individuals in a plain room with a single piece of furniture. Inside the room, they experience aural and visual stimuli, as well as electrical shocks. 42 out of the 55 participants said they would pay to not experience the shocks again. Then the participants were told to just sit and think, not experiencing any stimuli. They were given a button that would administer a mild shock to them when they pressed it. 1/3 of the males and 1/4 of the females pressed the button to give themselves a shock to distract from the boredom.
- Research by Virginia Tech in 2014 showed that the mere presence of a phone on a table, even if not being used or monitored, affected the quality of attention and conversation between participants sitting at the table.
- Working as a professor at MIT, Sherry Turkle found that students more and more preferred e-mailing questions instead of coming by for office hours. Taking the time to finely craft a perfect e-mail took precedence over talking in person, and potentially having imperfect speech.
- Alex Soojung-Kim Pang’s tips for not letting a mobile device dominate your life: 
	- 1. Turn off non-vital notifications.
	- 2. Set up so that only vital notifications that you need get shown.
	- 3. Fight phantom gadget syndrome by keeping the phone in a bag or elsewhere.
	- 4. Remember to breath when waiting for things to load.
- Linda Henkel, of Fairfield University, found that people who take photos of objects or places to better remember them are worse at remembering specific details about what they photographed.
- The areas of the brain that are under-stimulated due to depression are over-stimulated when playing video games.
- The same brain mechanisms at work during meditation are also at work during focused video game playing.
- Many European countries have laws mandating at least 4 workweeks worth of paid vacation. The United States does not have a federal law mandating any amount of paid vacation time.
- Research by Dr. Mary Helen Immordino-Yang indicated that children who multitask with social media more are less open with their feelings and less understanding of the feelings of others.
- According to research by Dr. Judson Brewer, different mindfulness techniques can be either helpful or hurtful to mind-wandering. Techniques that focus your attention on breathing can hurt mind-wandering, whereas techniques that let you process and think about outside experiences will help.

### Quotes

> There is no question that we are at an unprecedented point in history, where our attention is in hot demand. With the advent of smartphones and tablets, mobile consumers now spend an average of two hours and fifty-seven minutes each day on mobile devices and about eleven hours a day in front of a screen. Although we don’t know if all this screen time will have loner-term harmful effects, we know technology is changing us (and it’s unclear whether it’s for the better).

<div></div>

> Golden Krishna, a expert in user experience who currently works on design strategy at Google, astutely pointed out [...] that the only people who refer to their customers as “users” are drug dealers — and technologists.

<div></div>

> Having worked for innovation labs at Samsung and Zappos, Krishna witnessed firsthand this shift in priorities from the historic definition of design as “solving people’s core problems in an elegant way” to adding other features that “intentionally get you hooked.”

### Further Reading

- Pang, Alex Soojung-Kim - *The Distraction Addiction*
- Wolf, Maryanne - *Letters to the Good Reader: The Future of the Reading Brain in a Digital World*
- Krishna, Golden - *The Best Interface Is No Interface*
